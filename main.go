package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
)

type User struct {
	ID             string
	Name           string
	CurrentStatus  Status
	PreviousStatus Status
	Updated        time.Time
}

type Status int

const (
	Online Status = iota
	Idle
	DoNotDisturb
	Offline
	Unknown
)

var users []User

func main() {
	// Read .env file
	env, err := godotenv.Read(".env")
	if err != nil {
		log.Fatal("Error loading .env file: ", err)
	}
	discordBotToken := env["DISCORD_BOT_TOKEN"]

	// Initiate Discord Bot
	bot, err := discordgo.New("Bot " + discordBotToken)
	if err != nil {
		fmt.Println("Error creating Discord session,", err)
		return
	}

	bot.AddHandler(func(s *discordgo.Session, m *discordgo.PresenceUpdate) {
		var currentStatus Status
		var userExists bool
		if m.Status == "online" {
			currentStatus = Online
		} else if m.Status == "idle" {
			currentStatus = Idle
		} else if m.Status == "dnd" {
			currentStatus = DoNotDisturb
		} else if m.Status == "offline" {
			currentStatus = Offline
		}

		// check if user is already in the list
		for _, user := range users {
			if user.ID == m.User.ID {
				user.PreviousStatus = user.CurrentStatus
				user.CurrentStatus = currentStatus
				user.Updated = time.Now()
				userExists = true
				return
			}
		}

		// if user is not in the list, append it
		if !userExists {
			users = append(users, User{
				ID:             m.User.ID,
				Name:           m.User.Username,
				CurrentStatus:  currentStatus,
				PreviousStatus: Unknown,
				Updated:        time.Now(),
			})
		}
	})

	//TODO: Output to webserver. LaMetric requests up to 10 users, return only their status.
	http.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		user1 := query.Get("user1")
		user2 := query.Get("user2")
		user3 := query.Get("user3")
		user4 := query.Get("user4")
		user5 := query.Get("user5")
		user6 := query.Get("user6")
		user7 := query.Get("user7")
		user8 := query.Get("user8")
		user9 := query.Get("user9")
		user10 := query.Get("user10")

		//rewrite this as a loop
	})

	http.ListenAndServe(":6972", nil) //turn to https later
}
